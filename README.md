# Stardate Calculator

stardate: a Star Trek star date calculator
it can use different formulas for calculation
also, it can calculate the date in
Warhammer 40k format

## Sources:

https://memory-alpha.fandom.com/wiki/Stardate <br>
http://trekguide.com/Stardates.htm#Today <br>
https://warhammer40k.fandom.com/wiki/Imperial_Dating_System <br>

## How to use

```
$ python3 stardate [-h] [-f {tog,tng,wh40k}]
```


## optional arguments:
  -h, --help          show this help message and exit <br>
  -f {tog,tng,wh40k}  stardate format <br>

Where tog means The Original Series, TNG is The Next Generation and WH40k 
is Warhammer 40k


