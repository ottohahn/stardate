#!/usr/bin/env python
"""
stardate: a Star Trek star date calculator
it can use different formulas for calculation
also, it can calculate the date in
Warhammer 40k format
Sources:
https://memory-alpha.fandom.com/wiki/Stardate
http://trekguide.com/Stardates.htm#Today
https://warhammer40k.fandom.com/wiki/Imperial_Dating_System
"""
import argparse
from datetime import date
from datetime import datetime

def stardate_simple(ad):
    """
    Represent the current date in YYMM.DD format, where "YY" is the
    current year minus 1900, MM is the current month (01-12), and DD is
    the current day of the month (01-31)
    for newer dates more than 1999 the whole year will be used
    """
    stardate = str(ad.year)[-3:]+str(ad.month).zfill(2)+'.'+str(ad.day)
    return(stardate)

def stardate_new(ad):
    """
    Revised format for the calculations
    using the reference:
    """
    doy = ad.timetuple().tm_yday
    sd = doy / 365.
    stardate = str(ad.year)+'.'+str(round(sd,2))[-2:]
    return stardate

def wh40k(ad):
    """
    Representation of the date using the
    WH40k format <check> <year fraction> <year> M<millenia>
    the date this way does not take into account the hour
    (which should or could be taken into account)
    """
    yr_mill = ad.timetuple().tm_yday / 1000
    wh = '0.' + str(yr_mill)[-3:]+'.'+ str(ad.year)[-3:] + ' M'+ str(ad.year)[0]
    return wh


def main(ad):
    """
    Main entry point of the program
    TO DO:
    - option to give a date and calculate the stardate
    - use datetimes instead of dates
    """
    parser = argparse.ArgumentParser(
                    prog = 'stardate',
                    description = 'Stardate and WH40k date calculator',
                    epilog = 'And they shall know no fear')
    parser.add_argument('-f', choices=['tog', 'tng','wh40k'],
                    default='tog',
                    help='stardate format')
    args = parser.parse_args()
    if args.f == 'tog':
        return stardate_simple(ad)
    elif args.f == 'tng':
        return stardate_new(ad)
    elif args.f == 'wh40k':
        return wh40k(ad)
    else:
        return 'Error unknown stardate format'


if __name__=="__main__":
    d = date.today()
    print(main(d))
